package com.progettofinale.store.services;

import java.sql.SQLException;
import java.util.ArrayList;

public interface Dao<T> {
	
	T getById(int ID) throws SQLException;
	T getByCod(String codice) throws SQLException;
	ArrayList<T> getAll() throws SQLException;
	boolean insert(T t) throws SQLException;
	boolean delete(T t) throws SQLException;
	boolean update(T t) throws SQLException;

}