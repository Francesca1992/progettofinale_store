package com.progettofinale.store;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.progettofinale.store.models.Ordine;
import com.progettofinale.store.models.Prodotto;
import com.progettofinale.store.models.Store;
import com.progettofinale.store.models.Utente;
import com.progettofinale.store.services.OrdineDao;
import com.progettofinale.store.services.ProdottoDao;
import com.progettofinale.store.services.StoreDao;
import com.progettofinale.store.services.UtenteDao;

@RestController
public class StoreController {
	
	//Registrazione nuovo utente 
	@PostMapping("/inserisciutente")
	public boolean inserisciUtente(@RequestBody Utente objUtente) {
	
		boolean inserimento = false;
		
		UtenteDao utenteDao = new UtenteDao();
		
		try {
			inserimento = utenteDao.insert(objUtente);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return inserimento;
	}
	
	//Autenticazione Utente
	@PostMapping("/verificalogin")
	public boolean verificaLogin(@RequestBody Utente objUtente) {
		
		boolean login = false;
		
		UtenteDao utenteDao = new UtenteDao();
		Utente utenteTemp = new Utente();
		
		try {
			utenteTemp = utenteDao.getByUser(objUtente.getUsername());
			if (utenteTemp != null) {
				login = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return login;
		
	}
	
	//Utente modifica i suoi dati
	@PutMapping("/modificautente")
	public boolean modificaUtente(@RequestBody Utente objUtente) {
		
		boolean modifica = false;
		
		UtenteDao utenteDao = new UtenteDao();
		Utente temp = new Utente();
		
		try {
			temp = utenteDao.getByUser(objUtente.getUsername());
			temp.setPsw(objUtente.getPsw());
			temp.setIndirizzo(objUtente.getIndirizzo());
			
			modifica = utenteDao.update(temp);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	
		return modifica;
	}
	
	//Utente visualizza tutti gli store
	@GetMapping("/store")
	public ArrayList<Store> ricercaTuttiStore() {
		
		StoreDao storeDao = new StoreDao();
			
		ArrayList<Store> elenco = new ArrayList<Store>();
			
		try {
			elenco = storeDao.getAll();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return elenco;
			
	}

	//Utente seleziona uno store
	@GetMapping("/store/{codice}")
	public Store ricercaStorePerCodice(@PathVariable String codice) {
		
		StoreDao storeDao = new StoreDao();
		
		Store temp = null;
		
		try {
			temp = storeDao.getByCod(codice);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return temp;
		
	}
		
	//Utente visualizza tutti i suoi ordini								da rivedere				anche Store deve vedere ordini che gli sono stati fatti
	@GetMapping("/ordine")
	public ArrayList<Ordine> ricercaTuttiOrdini(){
		
		OrdineDao ordineDao = new OrdineDao();
		
		ArrayList<Ordine> elenco = new ArrayList<Ordine>();
		
		try {
			elenco = ordineDao.getAll();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return elenco;
		
	}
	
	//Utente visualizza un suo ordine									ci vuole un controllo che lo faccia entrare solo nei suoi ordini
	@GetMapping("/ordine/{id}")
	public Ordine ricercaOrdinePerId(@PathVariable Integer id) {
		
		OrdineDao ordineDao = new OrdineDao();
		
		Ordine temp = null;
		
		try {
			temp = ordineDao.getById(id);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return temp;
		
	}
	
	//Utente o Store inserisce un nuovo ordine
	@GetMapping("/inserisciordine")
	public boolean inserisciOrdine(@RequestBody Ordine objOrdine) {

		boolean inserimento = false;
		
		OrdineDao ordineDao = new OrdineDao();
		
		try {
			inserimento = ordineDao.insert(objOrdine);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return inserimento;
	}
	
	//Store modifica un ordine
	@PutMapping("/modificaordine")
	public boolean modificaOrdine(@RequestBody Ordine objOrdine) {
		
		boolean modifica = false;
		
		OrdineDao ordineDao = new OrdineDao();
		Ordine temp = new Ordine();
		
		try {
			temp = ordineDao.getByCod(objOrdine.getCodiceOrdine());
			temp.setOrario(objOrdine.getOrario());
			temp.setUtenterif(objOrdine.getUtenterif());
			
			modifica = ordineDao.update(temp);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	
		return modifica;
	}
	
	
	//Store o Utente visualizza tutti prodotti dello Store
	@GetMapping("/prodotto/{storeid}")
	public ArrayList<Prodotto> ricercaTuttiProdotti(@PathVariable Integer storeid) {
		
		ProdottoDao prodottoDao = new ProdottoDao();
		StoreDao storeDao = new StoreDao();
		
		Store storeTemp = new Store();
		
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();		
		
		try {
			storeTemp = storeDao.getById(storeid);
			elenco = prodottoDao.getAllSingleStore(storeTemp);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return elenco;
		
	}
	
	//Store o Utente visualizza i dettagli di un prodotto
	@GetMapping("/prodotto/{id}")
	public Prodotto ricercaProdottoPerId(@PathVariable Integer id) {
		
		ProdottoDao proDao = new ProdottoDao();
		
		Prodotto temp = null;
		
		try {
			temp = proDao.getById(id);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return temp;
		
	}
	
	//Store elimina un suo prodotto
	@DeleteMapping("/prodotto/{id}")
	public boolean eliminaProdotto(@PathVariable Integer id) {
		
		boolean eliminazione = false;
		
		ProdottoDao prodottoDao = new ProdottoDao();
		Prodotto temp = new Prodotto();

		try {
			temp = prodottoDao.getById(id);
			if(temp != null) {
				eliminazione = prodottoDao.delete(temp);
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return eliminazione;
	}
	
	//Store inserisce un nuovo prodotto
	@GetMapping("/inserisciprodotto")
	public boolean inserisciProdotto(@RequestBody Prodotto objProdotto) {

		boolean inserimento = false;
		
		ProdottoDao prodottoDao = new ProdottoDao();
		
		try {
			inserimento = prodottoDao.insert(objProdotto);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return inserimento;
	}
	
	//Store modifica un suo prodotto
	@PutMapping("/modificaprodotto")
	public boolean modificaProdotto(@RequestBody Prodotto objProdotto) {
		
		boolean modifica = false;
		
		ProdottoDao prodottoDao = new ProdottoDao();
		Prodotto temp = new Prodotto();
		
		try {
			temp = prodottoDao.getByCod(objProdotto.getCodiceProdotto());
			temp.setNome(objProdotto.getNome());
			temp.setDescrizione(objProdotto.getDescrizione());
			temp.setQuantita(objProdotto.getQuantita());
			temp.setPrezzo(objProdotto.getPrezzo());
			temp.setStorerif(objProdotto.getStorerif());
			
			modifica = prodottoDao.update(temp);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	
		return modifica;
	}
	
}