package com.progettofinale.store.models;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Ordine {
	
	private int ordineid;
	private String codiceOrdine;
	private LocalDateTime orario;
	private int utenterif;
	private Utente user;
	private Store store;
	private ArrayList<Prodotto> elencoProdotti;
	
	public Ordine() {
		
	}

	public int getOrdineid() {
		return ordineid;
	}
	public void setOrdineid(int ordineid) {
		this.ordineid = ordineid;
	}
	public String getCodiceOrdine() {
		return codiceOrdine;
	}
	public void setCodiceOrdine(String codiceOrdine) {
		this.codiceOrdine = codiceOrdine;
	}
	public LocalDateTime getOrario() {
		return orario;
	}
	public void setOrario(LocalDateTime orario) {
		this.orario = orario;
	}
	public int getUtenterif() {
		return utenterif;
	}
	public void setUtenterif(int utenterif) {
		this.utenterif = utenterif;
	}
	public Utente getUser() {
		return user;
	}
	public void setUser(Utente user) {
		this.user = user;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public ArrayList<Prodotto> getElencoProdotti() {
		return elencoProdotti;
	}
	public void setElencoProdotti(ArrayList<Prodotto> elencoProdotti) {
		this.elencoProdotti = elencoProdotti;
	}
}