package com.progettofinale.store.models;

public class Prodotto {
	
	private int prodottoid ;
	private String nome;
	private String codiceProdotto;
	private String descrizione;
	private float prezzo;
	private int quantita;
	private int storerif;
	
	public Prodotto() {
		
	}
	
	public Prodotto(int prodottoid, String nome, String codiceProdotto, String descrizione, float prezzo, int quantita, int storerif) {
		this.prodottoid = prodottoid;
		this.nome = nome;
		this.codiceProdotto = codiceProdotto;
		this.descrizione = descrizione;
		this.prezzo = prezzo;
		this.quantita = quantita;
		this.storerif = storerif;
	}
	
	public int getProdottoid() {
		return prodottoid;
	}
	public void setProdottoid(int prodottoid) {
		this.prodottoid = prodottoid;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodiceProdotto() {
		return codiceProdotto;
	}
	public void setCodiceProdotto(String codiceProdotto) {
		this.codiceProdotto = codiceProdotto;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}
	public int getQuantita() {
		return quantita;
	}
	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}
	public int getStorerif() {
		return storerif;
	}
	public void setStorerif(int storerif) {
		this.storerif = storerif;
	}

}