package com.progettofinale.store.models;

import java.util.ArrayList;

public class Store {
	
	private int storeid;
	private String nome;
	private String codice;
	private String indirizzo;
	private String url_immagine;
	private ArrayList<Prodotto> elencoProdotti;
	
	public Store() {
		
	}
	
	public Store(int storeid, String nome, String codice, String indirizzo, String url_immagine, ArrayList<Prodotto> elencoProdotti) {
		
		this.storeid = storeid;
		this.nome = nome;
		this.codice = codice;
		this.indirizzo = indirizzo;
		this.url_immagine = url_immagine;
		this.elencoProdotti = elencoProdotti;
	}
	
	public int getStoreid() {
		return storeid;
	}
	public void setStoreid(int storeid) {
		this.storeid = storeid;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getUrl_immagine() {
		return url_immagine;
	}
	public void setUrl_immagine(String url_immagine) {
		this.url_immagine = url_immagine;
	} 
	public ArrayList<Prodotto> getElencoProdotti() {
		return elencoProdotti;
	}
	public void setElencoProdotti(ArrayList<Prodotto> elencoProdotti) {
		this.elencoProdotti = elencoProdotti;
	}
	
}