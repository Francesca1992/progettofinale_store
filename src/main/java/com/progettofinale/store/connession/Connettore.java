package com.progettofinale.store.connession;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class Connettore {

	private Connection conn;
	private static Connettore ogg_connessione;
	
	public static Connettore getIstanza() {
		if(ogg_connessione == null) {
			ogg_connessione = new Connettore();
			return ogg_connessione;
		}
		else {
			return ogg_connessione;
		}
	}
	
	public Connection getConnessione() throws SQLException{
		if(conn == null) {
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("127.0.0.1");
			dataSource.setPortNumber(3306);
			dataSource.setUser("root");
			dataSource.setPassword("toor");
			dataSource.setUseSSL(false);
			dataSource.setDatabaseName("progetto_finale");
			dataSource.setAllowPublicKeyRetrieval(true);
			
			conn = dataSource.getConnection();
			return conn;
		}
		else {
			return conn;
		}
	}
}
